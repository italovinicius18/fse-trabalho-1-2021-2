#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
#include <wiringPiI2C.h>
#include <wiringPi.h>
#include <softPwm.h>
#include <time.h>

#include "uart/uart.h"
#include "lcd/i2clcd.h"
#include "bme/bme280.h"
#include "bme/bmeutils.h"
#include "PID/pid.h"

// Comandos gerais
unsigned char solicitaTempInterna[7] = {0x01, 0x23, 0xC1, 2, 6, 5, 6};
unsigned char solicitaTempPotenciometro[7] = {0x01, 0x23, 0xC2, 2, 6, 5, 6};
unsigned char leComandoUsuario[7] = {0x01, 0x23, 0xC3, 2, 6, 5, 6};

unsigned char sinalControleInt[7] = {0x01, 0x16, 0xD1, 2, 6, 5, 6};
unsigned char sinalReferenciaFloat[7] = {0x01, 0x16, 0xD2, 2, 6, 5, 6};

unsigned char estadoDoSistemaLigar[8] = {0x01, 0x16, 0xD3, 2, 6, 5, 6, 1};
unsigned char estadoDoSistemaDesligar[8] = {0x01, 0x16, 0xD3, 2, 6, 5, 6, 0};
unsigned char modoDeControlePotenciometro[8] = {0x01, 0x16, 0xD4, 2, 6, 5, 6, 0};
unsigned char modoDeControleCurvaReflow[8] = {0x01, 0x16, 0xD4, 2, 6, 5, 6, 1};

// Variaveis gerais
float tempExterna, tempInterna, tempPotenciometro;
double sinalPID;
int timer = 0, modo = 0, sistema = 0;

int curvaTempo[10] = {0, 60, 120, 240, 260, 300, 360, 420, 480, 600};
int curvaTemperatura[10] = {25, 38, 46, 54, 57, 61, 63, 54, 33, 25};
int indiceCurva = 0;

#define I2C_ADDR 0x27 // I2C device address
// Define some device constants
#define LCD_CHR 1  // Mode - Sending data
#define LCD_CMD 0  // Mode - Sending command
#define LINE1 0x80 // 1st line
#define LINE2 0xC0 // 2nd line
int fd;            // seen by all subroutines in lcd

// Variavel relacionada ao BME280
struct bme280_dev dev;

// Define-se os pinos para cada dispositivo

const int PWMpinRes = 4; // WiringPI GPIO 23
const int PWMpinVet = 5; // WiringPI GPIO 24

void desligaForno()
{
    if (requestSignal(estadoDoSistemaDesligar))
    {
        printf("Forno desligado!\n");
        sistema = 0;
        timer = 0;
    }
    else
    {
        printf("Erro ao desligar forno :(\n");
    }
}

void ligaForno()
{
    if (requestSignal(estadoDoSistemaLigar))
    {
        printf("Forno ligado!\n");
        sistema = 1;
    }
    else
    {
        printf("Erro ao ligar forno :(\n");
    }
}

void defineValorReferencia(float tempReferencia)
{

    // sinalReferenciaFloat[7] = tempReferencia;

    if (sendFloat(sinalReferenciaFloat, tempReferencia))
    {
        printf("Valor de temperatura de referencia definido\n");
    }
    else
    {
        printf("Erro ao definir temperatura de referencia :(\n");
    }
}

void ativaPotenciometro()
{
    if (requestSignal(modoDeControlePotenciometro))
    {
        printf("Modo potenciometro ativado\n");
        modo = 0;
        timer = 0;
        indiceCurva = 0;
    }
    else
    {
        printf("Erro ao ativar potenciometro :(\n");
    }
}

void ativaCurvaReflow()
{
    if (requestSignal(modoDeControleCurvaReflow))
    {
        printf("Modo de curva reflow ativado\n");
        modo = 1;
        timer = 0;
    }
    else
    {
        printf("Erro ao ativar curva reflow :(\n");
    }
}

// O programa deve tratar a interrupção do teclado (Ctrl + C = sinal SIGINT) encerrando todas as comunicações com periféricos (UART / I2C / GPIO) e desligar os atuadores (Resistor e Ventoinha);
void desligarSistema(int sig)
{
    // printf("Caught signal %d\n", sig);
    desligaForno();
    softPwmWrite(PWMpinRes, 0);
    delay(0.7);
    softPwmWrite(PWMpinVet, 0);
    delay(0.7);
    ClrLcd();
    printf("Sistema desligado!\n");
    exit(0);
}

int curvaTemp(int timer){
    if(timer==curvaTempo[1]){
        defineValorReferencia(curvaTemperatura[1]);
        pid_atualiza_referencia(curvaTemperatura[1]);
        indiceCurva = 1;
    }
    else if(timer==curvaTempo[2]){
        defineValorReferencia(curvaTemperatura[2]);
        pid_atualiza_referencia(curvaTemperatura[2]);
        indiceCurva = 2;
    }
    else if(timer==curvaTempo[3]){
        defineValorReferencia(curvaTemperatura[3]);
        pid_atualiza_referencia(curvaTemperatura[3]);
        indiceCurva = 3;
    }
    else if(timer==curvaTempo[4]){
        defineValorReferencia(curvaTemperatura[4]);
        pid_atualiza_referencia(curvaTemperatura[4]);
        indiceCurva = 4;
    }
    else if(timer==curvaTempo[5]){
        defineValorReferencia(curvaTemperatura[5]);
        pid_atualiza_referencia(curvaTemperatura[5]);
        indiceCurva = 5;
    }
    else if(timer==curvaTempo[6]){
        defineValorReferencia(curvaTemperatura[6]);
        pid_atualiza_referencia(curvaTemperatura[6]);
        indiceCurva = 6;
    }
    else if(timer==curvaTempo[7]){
        defineValorReferencia(curvaTemperatura[7]);
        pid_atualiza_referencia(curvaTemperatura[7]);
        indiceCurva = 7;
    }
    else if(timer==curvaTempo[8]){
        defineValorReferencia(curvaTemperatura[8]);
        pid_atualiza_referencia(curvaTemperatura[8]);
        indiceCurva = 8;
    }
    else if(timer==curvaTemperatura[9]){
        defineValorReferencia(curvaTemperatura[0]);
        pid_atualiza_referencia(curvaTemperatura[0]);
        indiceCurva = 0;
    }
}

int main()
{
    // Iniciamos a UART para futuros comandos
    initUart();

    // Tratamos o sinal do Ctrl+c
    signal(SIGINT, desligarSistema);

    // LCD
    if (wiringPiSetup() == -1)
        exit(1);

    fd = wiringPiI2CSetup(I2C_ADDR);

    lcd_init(); // setup LCD
    ClrLcd();

    // BME
    if (initBME(&dev) > 0)
        printf("BME280 iniciada\n");

    // Tratamos os dispositivos conectados ao GPIO
    if (softPwmCreate(PWMpinRes, 0, 100) == 0)
    {
        printf("PWM do resistor criado!\n");
    }
    if (softPwmCreate(PWMpinVet, 0, 100) == 0)
    {
        printf("PWM da ventoinha criado!\n");
    }

    // Configurar constantes
    pid_configura_constantes(20.0, 0.1, 100.0);

    // Inicia o sistema desligado, no modo Potenciometro e com o LCD falando que o sistema está desligado
    desligaForno();
    ativaPotenciometro();

    tempPotenciometro = requestFloat(solicitaTempPotenciometro);
    pid_atualiza_referencia(tempPotenciometro);

    while (1)
    {
        // Variaveis que vao armazenar o tempo
        time_t t = time(NULL);   
        struct tm tm = *localtime(&t);

        // Solicitamos todas as variaveis a cada periodo
        tempExterna = tempData(&dev);
        tempInterna = requestFloat(solicitaTempInterna);
        tempPotenciometro = requestFloat(solicitaTempPotenciometro);
        int comandoUsuario = requestInt(leComandoUsuario);

        // Verificamos se os dados estão consistentes no terminal
        printf("Tempo %d s\n", timer);
        // printf("Temp interna: %f\n", tempInterna);
        // printf("Temp externa %f\n", tempExterna);
        // printf("Temp Potenciometro: %f\n", tempPotenciometro);
        // printf("Temp Referencia: %d\n", curvaTemperatura[indiceCurva]);
        printf("Comando usuario: %d\n", comandoUsuario);

        // Tratamento do comando do usuario
        switch (comandoUsuario)
        {
        case 1:
            ligaForno();
            break;

        case 2:
            desligaForno();
            ClrLcd();
            break;

        case 3:
            ativaPotenciometro();
            break;

        case 4:
            ativaCurvaReflow();
            break;

        default:
            break;
        }

        switch (modo)
        {
        case 0: // Modo potenciometro
            printf("Modo Potenciometro\n");
            pid_atualiza_referencia(tempPotenciometro);
            break;

        case 1: // Modo curva reflow
            curvaTemp(timer);
            printf("Modo Curva reflow\n");
            break;

        default:
            break;
        }

        // Tratamento dos modos de operacao
        sinalPID = pid_controle(tempInterna);
        sendInt(sinalControleInt,sinalPID);
        printf("Sinal PID: %lf\n", sinalPID);

        if (sinalPID < 0)
        {
            softPwmWrite(PWMpinRes, 0);
            delay(0.7);
            softPwmWrite(PWMpinVet, sinalPID * -1);
            delay(0.7);
        }
        else if (sinalPID > 0)
        {
            softPwmWrite(PWMpinVet, 0);
            delay(0.7);
            softPwmWrite(PWMpinRes, sinalPID);
            delay(0.7);
        }

        timer++;

        FILE *fpt;

        fpt = fopen("records.csv", "a+");

        if (modo == 0)
        {
            fprintf(fpt, "%02d/%02d/%d;%02d:%02d:%02d;%0.2lf;%0.2lf;%0.2lf;%0.2lf\n", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec, tempInterna, tempExterna, tempPotenciometro, sinalPID);
            lcdLoc(LINE1);
            typeln("POT.");
            typeln(" TI:");
            typeFloat(tempInterna);
            lcdLoc(LINE2);
            typeln("TE:");
            typeFloat(tempExterna);
            typeln(" TR:");
            typeFloat(tempPotenciometro);
            printf("POT: %02d/%02d/%d;%02d:%02d:%02d;%0.2lf;%0.2lf;%0.2lf;%0.2lf\n", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec, tempInterna, tempExterna, tempPotenciometro, sinalPID);
        }
        else
        {
            fprintf(fpt, "%02d/%02d/%d;%02d:%02d:%02d;%0.2lf;%0.2lf;%d;%0.2lf\n", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec, tempInterna, tempExterna, curvaTemperatura[indiceCurva], sinalPID);
            lcdLoc(LINE1);
            typeln("CURVA");
            typeln(" TI:");
            typeFloat(tempInterna);
            lcdLoc(LINE2);
            typeln("TE:");
            typeFloat(tempExterna);
            typeln(" TR:");
            typeFloat(curvaTemperatura[indiceCurva]);
            printf("CURVA: %02d/%02d/%d;%02d:%02d:%02d;%0.2lf;%0.2lf;%d;%0.2lf\n", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec, tempInterna, tempExterna, curvaTemperatura[indiceCurva], sinalPID);
        }

        fclose(fpt);
    }

    closeUart();
    return 0;
}