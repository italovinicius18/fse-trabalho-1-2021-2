raspbian: 
	gcc main.c uart/uart.c crc/crc16.c bme/bme280.c bme/bmeutils.c lcd/i2clcd.c PID/pid.c -I ../ -lwiringPi -o bin

linux: 
	gcc main.c uart/uart.c crc/crc16.c bme/bme280.c bme/bmeutils.c lcd/i2clcd.c PID/pid.c -I ../ -lwiringPi -o bin

clean:
	rm -v bin