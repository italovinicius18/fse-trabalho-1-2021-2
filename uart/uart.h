#ifndef _UART_
#define _UART_

void initUart();
void closeUart();
int sendFloat(char cmd[],float f);
int sendInt(char cmd[], int x);
int sendSignal(char cmd[]);
int requestSignal(char cmd[]);
int requestInt(char cmd[]);
float requestFloat(char cmd[]);

#endif