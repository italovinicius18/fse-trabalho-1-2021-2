# FSE - Trabalho 1 - 2021-2

Repositório focado no trabalho 1 da matéria de Fundamentos de sistemas embarcados feito no Rasp43

## Objetivo

Este trabalho tem por objetivo a implementação de um sistema (que simula) o controle de um forno para soldagem de placas de circuito impresso (PCBs). Abaixo vemos alguns exemplos de fornos comerciais para este propósito.

## Descrição

Por meio das bibliotecas em C listadas abaixo, consegui manipular os dispositivos disponíveis para que assim manipulassem em dois tipos de operação o forno de soldagem.

 - stdlib.h
 - stdio.h
 - signal.h
 - wiringPi.h
 - wiringPiI2C.h
 - unistd.h
 - softPwm.h
 - time.h
 - linux/types.h
 - linux/kernel.h
 - linux/i2c-dev.h
 - stdint.h
 - stddef.h
 - sys/ioctl.h
 - sys/types.h
 - string.h
 - fcntl.h
 - termios.h

O funcionamento se dá por meio da dashboard disponibilizada pelo professor e assim a cada manipulação o código trata estes eventos.

Existem dois modos para controlar o forno:

- Modo Potenciômetro
- Modo Curva reflow

O sistema inicia-se desligado e no modo potenciômetro, ao ligar o sistema, as informações referentes ao modo de funcionamento e temperatura serão apresentadas no LCD.

Todas as informações de funcionamento e comandos de usuário são apresentados no terminal. inclusive o timer que reinicia a cada vez que o modo de funcionamento é alterado

Para encerrar o programa, deve pressionar as teclas Ctrl+c, assim o forno é desligado e o resistor e ventoinha são normalizados.

## Resultados

As informações necessárias para plotagem dos gráficos estão apresentadas na pasta "data" e foram movidas dos dados registrados no arquivo "records.csv" que é populado ao executar o código.

A partir destes dados, obtive os gráficos dos dois modos de funcionamento:

### Modo Potenciômetro

#### Gráfico de temperaturas

![Gráfico de temperaturas do modo potenciômetro](/assets/potTemp.png)

#### Gráfico dos sinais enviados

![Gráfico de sinais do modo potenciômetro](/assets/potSinal.png)

### Modo Curva reflow

#### Gráfico de temperaturas

![Gráfico de temperaturas da curva reflow](/assets/curvaTemp.png)

#### Gráfico dos sinais enviados

![Gráfico de sinais da curva reflow](/assets/curvaSinal.png)


Os gráficos foram feitos por meio da ferramenta PowerBI.

## Requisitos

Para executar o código necessita-se de um ambiente raspbian, por isso é necessário conectar á uma Raspberry Pi4, cuja qual foi utilizada para desenvolver este projeto.

Além do que precisa-se configurar o ambiente exatamente igual e com os mesmos códigos apresentados neste <a href="https://gitlab.com/fse_fga/trabalhos-2021_2/trabalho-1-2021-2/-/tree/main" target="_blank">repositório</a> 

## Uso

Para executar o código apresentado , precisa-se clonar este repositório. acessar a pasta clonada e executar o comando abaixo:

```sh
make && ./bin
```

## Autor

Ítalo Guimarães.

## Licença

Este repositório não se encontra sob nenhuma licença